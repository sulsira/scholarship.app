<nav>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="./Dashboard Template for Bootstrap_files/Dashboard Template for Bootstrap.htm">Scholarships</a>
    </div>

    <div class="navbar-collapse collapse">

      <ul class="nav navbar-nav navbar-right">
        <li><a href="./Dashboard Template for Bootstrap_files/Dashboard Template for Bootstrap.htm">Students</a></li>
        <li><a href="./Dashboard Template for Bootstrap_files/Dashboard Template for Bootstrap.htm">Interviews</a></li>
        <li><a href="./Dashboard Template for Bootstrap_files/Dashboard Template for Bootstrap.htm">Awards</a></li>
        <li><a href="./Dashboard Template for Bootstrap_files/Dashboard Template for Bootstrap.htm">Head counts</a></li>
        <li><a href="./Dashboard Template for Bootstrap_files/Dashboard Template for Bootstrap.htm">Codes</a></li>
        <li class="dropdown">    
  <a href="#" class="dropdown-toggle" data-toggle="dropdown">User name <span class="caret"></span></a>
  <ul class="dropdown-menu" role="menu">
    <li><a href="#">Dashboard</a></li>
    <li><a href="#">Settings</a></li>
    <li><a href="#">Users</a></li>
    <li><a href="#">Profile</a></li>
    <li class="divider"></li>
    <li class="dropdown-header">User sessions</li>
    <li><a href="#">Log out</a></li>
    <!-- <li><a href="#">One more separated link</a></li> -->
  </ul>
</li>
      </ul>
    </div>
  </div>
</div>  			
</nav>