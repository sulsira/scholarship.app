@include('templates/top')
  		<div class="wrapper">
  			<div class="content">
			    <div class="container-fluid">
			      <div class="row">
			      	@include('__partials/main-sidebar')
			      	<div class="content-area">

			        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main"> <!-- here -->
			      		<div class="page-header">
							<div class="blog-masthead">
						      <div class="clearfix">
						        <nav class="blog-nav">
						          <a class="blog-nav-item active" href="#">Dashboard</a>
						          <a class="blog-nav-item" href="#">Add students</a>
						          <a class="blog-nav-item" href="#">add interview</a>
						          <a class="blog-nav-item" href="#">add interviewers</a>
						          <a class="blog-nav-item" href="#">Checking</a>
<!-- 								    <form class="navbar-form navbar-right">
								      <input type="text" class="form-control" placeholder="Search...">
								      <button type="submit" class="btn btn-success">search</button>
								    </form>    -->  
						        </nav>
						      </div>
							</div>
			      			<h1>Dashboard</h1>
			      		</div>
			          <div class="row placeholders">
			            <div class="col-xs-6 col-sm-3 placeholder">
							<span class="mega-octicon octicon-organization"></span>
			              <h4>Students</h4>
			              <span class="text-muted">A list of students</span>
			            </div>
			            <div class="col-xs-6 col-sm-3 placeholder">
						<span class="mega-octicon octicon-diff"></span>
			              <h4>Adding</h4>
			              <span class="text-muted">Add students</span>
			            </div>
			            <div class="col-xs-6 col-sm-3 placeholder">
							<span class="mega-octicon octicon-mortar-board"></span>
			              <h4>Scholarship</h4>
			              <span class="text-muted">Add a scholarship</span>
			            </div>
			            <div class="col-xs-6 col-sm-3 placeholder">
							<span class="mega-octicon octicon-calendar"></span>
			              <h4>Interview</h4>
			              <span class="text-muted">Schedule an interview</span>
			            </div>
			          </div>
			          <h2 class="sub-header">Online Applicants</h2>
			          <div class="table-responsive">
			            <table class="table table-striped">
			              <thead>
			                <tr>
			                  <th>#</th>
			                  <th>Header</th>
			                  <th>Header</th>
			                  <th>Header</th>
			                  <th>Header</th>
			                </tr>
			              </thead>
			              <tbody>
			                <tr>
			                  <td>1,001</td>
			                  <td>Lorem</td>
			                  <td>ipsum</td>
			                  <td>dolor</td>
			                  <td>sit</td>
			                </tr>

			              </tbody>
			            </table>
			          </div>
			        </div> <!--conent inside wrapper -->
			      	</div>
			      </div>
			    </div>
  			</div><!-- end of content -->
  		</div><!-- end of wrapper -->	
@include('templates/bottom')